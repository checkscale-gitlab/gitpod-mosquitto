FROM gitpod/workspace-full:latest

RUN sudo apt update \
    && sudo apt install -y mosquitto \
    && sudo rm -rf /var/lib/apt/lists/*
